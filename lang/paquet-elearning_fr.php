<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-elearning
// Langue: fr
// Date: 03-09-2016 00:39:35
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'elearning_description' => 'Ce plugin permet d\'utiliser SPIP comme une plateforme simple de E-Learning.

Sur le principe, ce plugin est une aggréggation de plusieurs plugins, plus des fonctionnalités qui lui sont propres.
_ En plus d\'avoir ses cours en ligne, on peut donc faire des tests notés, voir les connexions et les pages visitées par les élèves, définir une progression dans les cours suivant les notes du cours précédent, etc.',
	'elearning_slogan' => 'Plate-forme simple de E-Learning',
);
?>